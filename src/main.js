import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Toaster, {timeout: 5000})
Vue.use(VuejsDialog);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
