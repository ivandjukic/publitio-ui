import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home/Home'
import People from './views/People/People'
import Profile from "./views/Profile/Profile";

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
            requireAuth: false
        },
        {
            path: '/people',
            name: 'people',
            component: People,
            requireAuth: false
        },
        {
            path: '/profile/:id',
            name: 'profile',
            component: Profile,
            requireAuth: false
        }
    ]
})
