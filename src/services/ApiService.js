import {apiClient} from '../utils/ApiClient';
import {isObjectEmpty} from '../utils/Helpers';
import router from './../router';


apiClient.interceptors.response.use((response) => {
    return response;
}, function (error) {
    if (error.response.status === 401) {
        router.push({name: 'home'});
        return Promise.reject(error)
    }
    return Promise.reject(error)
});


export default {
    get(resource, id = null, query_params = null) {
        if (id) {
            resource += '/' + id;
        }
        if (query_params) {
            resource += '?';
            for (let key in query_params) {
                resource += key + '=' + query_params[key];
                delete query_params[key];
                if (!isObjectEmpty(query_params)) {
                    resource += '&';
                }
            }

        }
        return apiClient.get(resource);
    },

    post(resource, data) {
        return apiClient.post(resource, data);
    },

    delete(resoruce, id) {
        var url = resoruce + '/' + id;
        return apiClient.delete(url);
    }
}
