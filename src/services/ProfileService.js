import ApiService from "../services/ApiService";

export default {
    fetchProfile(id) {
        return ApiService.get('profile', id);
    },

    delete(id) {
        return ApiService.delete('people', id);
    }
}
