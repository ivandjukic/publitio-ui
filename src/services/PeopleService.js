import ApiService from "../services/ApiService";
import store from '../store/store'

export default {
    fetchPeople(queryParams = null) {
        if (!queryParams) {
            queryParams = {
                limit: store.getters.peoplePerPage,
                offset: 0
            };
        }
        return ApiService.get('people', null, queryParams);
    },

    create(data) {
        return ApiService.post('people', data);
    },

    isEmailTaken(email) {
        return ApiService.get('isEmailTaken/' + email);
    },

    delete(id) {
        return ApiService.delete('people', id);
    }
}
