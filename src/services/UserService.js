import ApiService from "../services/ApiService";
import store from "../store/store";

export default {
    login(credentials) {
        return new Promise((resolve, reject) => {
            ApiService.post('login', credentials)
                .then(response => {
                    localStorage.setItem('jwtToken', response.data.token);
                    store.dispatch('loginSucceeded', response.data);
                    resolve('success');
                })
                .catch(error => {
                    localStorage.removeItem('jwtToken');
                    store.dispatch('loginFailed', 'Unexpected error, try again');
                    reject('fail');
                })
        })
    },

    getUser() {
        return ApiService.get('user');
    }
}
