import ApiService from "../services/ApiService";

export default {
    sendEmail(data) {
        return ApiService.post('email', data);
    }
}
