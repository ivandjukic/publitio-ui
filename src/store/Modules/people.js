export const state = {
    people: [],
    peoplePerPage: 10,
    currentPage: 1
};

export const mutations = {
    SET_PEOPLE(state, people) {
        state.people = people;
    },
    SET_CURRENT_PAGE(state, page) {
        state.page = page;
    }
};

export const actions = {
    setPeople({commit}, people) {
        commit('SET_PEOPLE', people);
    },
    setCurrentPage({commit}, page) {
        commit('SET_CURRENT_PAGE', page);
    }
};

export const getters = {
    people: state => state.people.profiles,
    numberOfPeople: state => state.people.total,
    peoplePerPage: state => state.peoplePerPage,
    currentPage: state => state.currentPage
};