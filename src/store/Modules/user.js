export const state = {
    isUserLoggedIn: false,
    jwtToken: localStorage.getItem('jwtToken') || '',
    loginError: null,

};

export const mutations = {
    LOGIN_SUCCEEDED(state, jwtToken) {
        state.jwtToken = jwtToken;
        state.isUserLoggedIn = true;
    },
    LOGIN_FAILED(state) {
        state.jwtToken = '';
        state.isUserLoggedIn = false;
    },
    LOGOUT(state) {
        state.jwtToken = '';
        state.isUserLoggedIn = false;
    },
    SET_LOGIN_ERROR(state, error) {
        state.loginError = error;
    }
};

export const actions = {
    logout({commit}) {
        localStorage.removeItem('jwtToken');
        commit('LOGOUT');
    },
    loginSucceeded({commit}, jwtToken) {
        commit('LOGIN_SUCCEEDED', jwtToken);
    },
    loginFailed({commit}, error) {
        commit('LOGIN_FAILED');
        commit('SET_LOGIN_ERROR', error);
    }
};

export const getters = {
    isUserLoggedIn: state => state.isUserLoggedIn,
    loginError: state => state.loginError,
    jwtToken: state => state.jwtToken,
};