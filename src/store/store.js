import Vue from 'vue'
import Vuex from 'vuex'
import * as user from './Modules/user'
import * as people from './Modules/people'

Vue.use(Vuex);

export const namespaced = true;
export default new Vuex.Store({
    modules: {
        user,
        people
    }
})