import axios from 'axios'
import config from '../config'

export const apiClient = axios.create({
    baseURL: config.backendApiUrl,
    withCredentials: false,
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: {
            toString() {
                return `Bearer ${localStorage.getItem('jwtToken')}`
            }
        }
    },
    timeout: 10000
});
